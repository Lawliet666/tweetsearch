package com.test.twitterhashtagsearch

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.test.twitterhashtagsearch.ui.search.TweetSearchFragment
import kotlinx.android.synthetic.main.main_activity.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        toolbar.setTitle(R.string.search_title)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, TweetSearchFragment.newInstance())
                    .commitNow()
        }
    }
}
