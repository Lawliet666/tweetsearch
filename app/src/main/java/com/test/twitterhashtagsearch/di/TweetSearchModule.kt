package com.test.twitterhashtagsearch.di

import com.test.twitterhashtagsearch.api.ErrorMessageHandler
import com.test.twitterhashtagsearch.api.Repository
import com.test.twitterhashtagsearch.ui.search.TweetSearchPresenter
import dagger.Module
import dagger.Provides

@Module
class TweetSearchModule {

    @Provides
    fun providePresenter(repository: Repository, errorMessageHandler: ErrorMessageHandler): TweetSearchPresenter {
        return TweetSearchPresenter(repository, errorMessageHandler)
    }
}