package com.test.twitterhashtagsearch.di

import com.test.twitterhashtagsearch.api.Repository
import com.test.twitterhashtagsearch.api.RepositoryImpl
import com.twitter.sdk.android.core.services.SearchService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StorageModule {
    @Singleton
    @Provides
    fun provideRepository(apiService: SearchService): Repository {
        return RepositoryImpl(apiService)
    }
}