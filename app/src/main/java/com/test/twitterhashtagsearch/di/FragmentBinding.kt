package com.test.twitterhashtagsearch.di

import com.test.twitterhashtagsearch.ui.search.TweetSearchFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector
import javax.inject.Scope

@Module
abstract class FragmentBinding {

    @PerFragment
    @ContributesAndroidInjector(modules = [TweetSearchModule::class])
    abstract fun provideTweetSearchFragment(): TweetSearchFragment

    @Scope
    @Retention(AnnotationRetention.RUNTIME)
    annotation class PerFragment
}