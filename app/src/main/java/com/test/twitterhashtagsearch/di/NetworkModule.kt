package com.test.twitterhashtagsearch.di

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.test.twitterhashtagsearch.api.ErrorMessageHandler
import com.twitter.sdk.android.core.TwitterCore
import com.twitter.sdk.android.core.services.SearchService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import org.threeten.bp.LocalDateTime
import javax.inject.Singleton

@Module
class NetworkModule {
    @Singleton
    @Provides
    fun provideApiService(): SearchService = TwitterCore.getInstance().guestApiClient.searchService

    @Singleton
    @Provides
    fun provideErrorHandler(context: Context) = ErrorMessageHandler(context)
}