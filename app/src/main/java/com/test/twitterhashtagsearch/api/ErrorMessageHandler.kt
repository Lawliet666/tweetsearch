package com.test.twitterhashtagsearch.api

import android.content.Context
import com.test.twitterhashtagsearch.R
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class ErrorMessageHandler(private val context: Context) {

    fun getMessage(e: Throwable): CharSequence {
        return when (e) {
            is UnknownHostException, is SocketTimeoutException -> context.getString(R.string.check_network_connection)
            else -> context.getString(R.string.error)
        }
    }
}