package com.test.twitterhashtagsearch.api

import com.twitter.sdk.android.core.models.Tweet
import io.reactivex.Completable
import io.reactivex.Observable

interface Repository {
    fun query(query: String): Observable<List<Tweet>>
    fun loadMore(): Completable
}