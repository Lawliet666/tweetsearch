package com.test.twitterhashtagsearch.api

import com.twitter.sdk.android.core.models.Tweet
import com.twitter.sdk.android.core.services.SearchService
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import retrofit2.HttpException

class RepositoryImpl(private val searchService: SearchService) : Repository {

    companion object {
        private const val PAGE_SIZE: Int = 20
    }

    private val subject: BehaviorSubject<List<Tweet>> = BehaviorSubject.createDefault(emptyList())
    private var cached = mutableListOf<Tweet>()
    private var lastQuery: String? = null

    override fun query(query: String): Observable<List<Tweet>> {
        if (lastQuery != query) {
            lastQuery = query
            cached.clear()
            return subject
                    .startWith(loadTweets().toObservable())
                    .startWith(emptyList<List<Tweet>>())
        } else {
            return subject
        }
    }

    override fun loadMore(): Completable {
        return loadTweets().ignoreElement()
    }

    private fun loadTweets(): Single<List<Tweet>> {
        return getTweets(lastQuery ?: "", start = cached.lastOrNull()?.id)
                .doOnSuccess {
                    cached.addAll(it)
                    subject.onNext(cached)
                }
    }

    private fun getTweets(query: String, start: Long? = null): Single<List<Tweet>> {
        return Single.fromCallable {
            val response = searchService.tweets(query,
                    null, null, null, null, PAGE_SIZE, null,
                    start, null, null).execute()
            if (response.isSuccessful) {
                val body = response.body()!!
                return@fromCallable body.tweets
            } else {
                throw HttpException(response)
            }
        }
    }
}