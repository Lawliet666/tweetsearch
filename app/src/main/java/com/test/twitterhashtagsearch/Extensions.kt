package com.test.twitterhashtagsearch

import android.content.Context
import io.reactivex.disposables.Disposable

fun Int.resourceName(context: Context = App.app): String = context.resources.getResourceName(this)

fun Disposable?.isNullOrDisposed() = this == null || isDisposed