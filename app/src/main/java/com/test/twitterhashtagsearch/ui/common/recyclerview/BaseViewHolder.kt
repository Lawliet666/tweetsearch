package com.test.twitterhashtagsearch.ui.common.recyclerview

import android.support.v7.widget.RecyclerView
import android.view.View

open class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    inline fun runIfHasPosition(action: () -> Unit) {
        if (adapterPosition != RecyclerView.NO_POSITION) {
            action.invoke()
        }
    }
}