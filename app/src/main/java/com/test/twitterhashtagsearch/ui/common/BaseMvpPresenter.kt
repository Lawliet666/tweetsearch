package com.test.twitterhashtagsearch.ui.common

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BaseMvpPresenter<V : MvpView> : MvpPresenter<V>() {
    private val presenterLifecycleDisposable = CompositeDisposable()
    private val viewLifecycleDisposable = CompositeDisposable()

    fun Disposable.untilDestroy(): Disposable {
        presenterLifecycleDisposable.add(this)
        return this
    }

    fun Disposable.untilDetach(): Disposable {
        viewLifecycleDisposable.add(this)
        return this
    }

    override fun detachView(view: V) {
        super.detachView(view)
        viewLifecycleDisposable.clear()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenterLifecycleDisposable.clear()
    }
}