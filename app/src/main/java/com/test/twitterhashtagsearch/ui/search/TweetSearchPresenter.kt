package com.test.twitterhashtagsearch.ui.search

import com.arellomobile.mvp.InjectViewState
import com.jakewharton.rxbinding2.InitialValueObservable
import com.test.twitterhashtagsearch.api.ErrorMessageHandler
import com.test.twitterhashtagsearch.api.Repository
import com.test.twitterhashtagsearch.isNullOrDisposed
import com.test.twitterhashtagsearch.ui.common.BaseMvpPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

@InjectViewState
class TweetSearchPresenter(private val repository: Repository,
                           private val errorMessageHandler: ErrorMessageHandler) : BaseMvpPresenter<TweetSearchView>() {

    private var loadMoreDisposable: Disposable? = null
    private var searchDisposable: Disposable? = null

    fun setSearchInputObservable(queryTextChanges: InitialValueObservable<CharSequence>) {
        searchDisposable = queryTextChanges.skipInitialValue()
                .filter { it.isNotEmpty() }
                .map { it.toString() }
                .debounce(600, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .distinctUntilChanged()
                .doOnNext { viewState.showLoadingState(visible = true) }
                .observeOn(Schedulers.io())
                .switchMap { repository.query(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext { viewState.showLoadingState(visible = false) }
                .doOnError { handleError(it) }
                .onErrorReturnItem(emptyList())
                .subscribe({
                    viewState.showTweets(it)
                }, { handleError(it) })
                .untilDetach()
    }

    fun loadMore() {
        if (loadMoreDisposable.isNullOrDisposed()) {
            loadMoreDisposable = repository.loadMore()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { viewState.showLoadingState(visible = true) }
                    .doOnTerminate { viewState.showLoadingState(visible = false) }
                    .subscribe({ }, { handleError(it) })
        }

    }

    private fun handleError(it: Throwable) {
        it.printStackTrace()
        val message = errorMessageHandler.getMessage(it)
        viewState.showError(message)
    }
}