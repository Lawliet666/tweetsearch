package com.test.twitterhashtagsearch.ui.search

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.twitter.sdk.android.core.models.Tweet

interface TweetSearchView : MvpView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showTweets(items: List<Tweet>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showError(message: CharSequence)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showLoadingState(visible: Boolean)
}