package com.test.twitterhashtagsearch.ui.common.recyclerview

import android.app.Activity
import android.support.v7.widget.RecyclerView
import com.test.twitterhashtagsearch.KeyboardUtil
import java.lang.ref.WeakReference

class HideKeyboardOnScroll(activity: Activity) : RecyclerView.OnScrollListener() {
    private var activityWeakReference = WeakReference(activity)

    override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
        val activity = activityWeakReference.get()
        if (newState != RecyclerView.SCROLL_STATE_IDLE && activity != null) {
            KeyboardUtil.hideKeyboard(activity)
        }
    }

    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {

    }
}