package com.test.twitterhashtagsearch.ui.search

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.test.twitterhashtagsearch.R
import com.test.twitterhashtagsearch.ui.common.recyclerview.BaseViewHolder
import com.twitter.sdk.android.core.models.Tweet

class TweetAdapter : ListAdapter<Tweet, RecyclerView.ViewHolder>(diffCallback) {

    private var loading: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            R.id.recycler_type_simple_tweet -> {
                val view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false)
                TweetViewHolder(view)
            }
            R.id.footer -> createProgressIndicatorHolder(inflater, parent)
            else -> throw IllegalArgumentException("Unsupported view type")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (loading && position == itemCount - 1) {
            R.id.footer
        } else {
            R.id.recycler_type_simple_tweet
        }
    }

    private fun createProgressIndicatorHolder(inflater: LayoutInflater, parent: ViewGroup): RecyclerView.ViewHolder {
        val view = inflater.inflate(R.layout.recycler_view_progress, parent, false)
        return BaseViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewType = holder.itemViewType
        return when (viewType) {
            R.id.recycler_type_simple_tweet -> {
                val tweet = getItem(position)
                (holder as TweetViewHolder).bind(tweet)
            }
            else -> {
            }
        }
    }

    override fun getItemCount(): Int {
        return if (loading)
            super.getItemCount() + 1
        else
            super.getItemCount()
    }


    fun showLoading() {
        loading = true
        notifyItemInserted(itemCount)
    }

    fun hideLoading() {
        loading = false
        notifyItemRemoved(itemCount)
    }

    class TweetViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val textView = itemView.findViewById<TextView>(android.R.id.text1)

        fun bind(tweet: Tweet) {
            textView.text = tweet.text
        }
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<Tweet>() {
            override fun areItemsTheSame(oldItem: Tweet, newItem: Tweet) = oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: Tweet, newItem: Tweet) = oldItem == newItem
        }
    }
}