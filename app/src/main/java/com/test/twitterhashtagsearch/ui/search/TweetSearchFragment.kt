package com.test.twitterhashtagsearch.ui.search

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.jakewharton.rxbinding2.support.v7.widget.RxSearchView
import com.test.twitterhashtagsearch.R
import com.test.twitterhashtagsearch.ui.common.BaseMainFragment
import com.test.twitterhashtagsearch.ui.common.recyclerview.EndlessScrollListener
import com.test.twitterhashtagsearch.ui.common.recyclerview.HideKeyboardOnScroll
import com.twitter.sdk.android.core.models.Tweet
import kotlinx.android.synthetic.main.main_fragment.*
import javax.inject.Inject

class TweetSearchFragment : BaseMainFragment(), TweetSearchView {
    companion object {
        fun newInstance() = TweetSearchFragment()
    }

    private lateinit var endlessScrollListener: EndlessScrollListener

    @Inject
    @InjectPresenter
    lateinit var presenter: TweetSearchPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    private lateinit var adapter: TweetAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = TweetAdapter()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvContentList.adapter = adapter
        val layoutManager = LinearLayoutManager(context)
        rvContentList.itemAnimator = null
        rvContentList.layoutManager = layoutManager
        rvContentList.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        endlessScrollListener = object : EndlessScrollListener(layoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                rvContentList.post { presenter.loadMore() }
            }
        }
        rvContentList.addOnScrollListener(HideKeyboardOnScroll(requireActivity()))
        rvContentList.addOnScrollListener(endlessScrollListener)
        presenter.setSearchInputObservable(RxSearchView.queryTextChanges(searchView))
    }

    override fun showTweets(items: List<Tweet>) {
        adapter.submitList(items)
    }

    override fun showError(message: CharSequence) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        endlessScrollListener.resetState()
    }

    override fun showLoadingState(visible: Boolean) {
        if (visible)
            adapter.showLoading()
        else
            adapter.hideLoading()
    }
}
