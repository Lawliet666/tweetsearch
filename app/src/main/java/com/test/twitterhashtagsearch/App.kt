package com.test.twitterhashtagsearch

import android.app.Activity
import android.app.Application
import android.support.v4.app.Fragment
import com.jakewharton.threetenabp.AndroidThreeTen
import com.test.twitterhashtagsearch.di.DaggerAppComponent
import com.twitter.sdk.android.core.Twitter
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import io.reactivex.plugins.RxJavaPlugins
import javax.inject.Inject


class App : Application(), HasActivityInjector, HasSupportFragmentInjector {

    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>
    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun activityInjector() = activityDispatchingAndroidInjector
    override fun supportFragmentInjector() = fragmentDispatchingAndroidInjector

    override fun onCreate() {
        super.onCreate()
        app = this
        DaggerAppComponent.builder()
                .create(this)
                .inject(this)
        AndroidThreeTen.init(this)
        Twitter.initialize(this)
        RxJavaPlugins.setErrorHandler {
            it.printStackTrace()
        }
    }

    companion object {
        /**
         *  Application haven't Content Provider and this used just for kotlin extension function
         */
        lateinit var app: App
    }
}